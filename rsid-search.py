#!/usr/bin/env python2.7


###################################################################################
# RSID Search, Use with Raw 23andMe Data
# Written by John Minton <cjohnweb@gmail.com>
# http://www.pythonjohn.com                              
#
# Copyright (c) 2016 John Minton
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
###################################################################################


# Usage: Pass a file to search and the RSID you want, and get a json object back with the associated data.
# To pass in arguements
import sys
# To convert results to JSON format
import json


# Check if args were passed
args = len(sys.argv)

if args < 3:
	print "\nPlease pass the filename and one or more RSIDs that you would like to find.\n\n"
	print "python rsid-search.py filename rsid1 rsid2 rsid3 [...]"
	print "\n\n"
	sys.exit()


# get argument list

filename = str(sys.argv[1]) # Grab string

args = [] # Init as list
for a in sys.argv[2:]:
	args.append(str(a))

print "\nSearching in file "+filename+" for the following RSIDs:"

# Set a new dict to hold RSID's and their string length
rsids = {}

# Loop through the args passed in
for rsid in args:
	# Print RSID's to screen for user confirmation 
        print rsid
	
	# Let's also get the length of each rsid, for use below in finding matches
	length = len(rsid)
	
	# Save the RSID and len to a new dict
	rsids[rsid] = length


#print rsids

print "\n\nHere are the matches found:\n"


# Use this list to hold our results
data = []
data.append("rsid chromosome position genotype".split())
# Set this function, where the line matching actually happens
def check_line(haystack_line,needle_rsids):
	global data

	# Ignore lines prefixed with #
	if haystack_line[0] != "#":

		# Match lines starting with the items in list arg 

		# For each rsid:
		for rsid in rsids:

			# Slice string to the length of the RSID being searched for
			line_slice = haystack_line[0:rsids[rsid]]

			# Check if sliced string matches any of the rsids
			if rsid == line_slice:

				# When a match is found remove white space from string and convert to a list.
				result = haystack_line.split()

				# Then add that list into the data object
				data.append(result)
			

# Here we open the file and pass each line to the function defined above, saving the results as we go
with open(filename) as genetics:
    for line in genetics:
        check_line(line,rsids)



# Print results
results = len(data) - 1

if results != 1:
	result_summary = str(results) + " results were found"
else:
	result_summary = str(results) + " result was found"

data.insert(0,[result_summary])

data = json.dumps(data)

# At the very end convert into JSON object and print to screen
print "\n", data, "\n\n"
