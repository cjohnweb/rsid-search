#!/usr/bin/env python2.7


# BioPython Test by John Minton <cjohnweb@gmail.com>
# http://pythonjohn.com/
# Creative Commons Attribution License
# Originally developed on Debian


# Notes on bioPytohn
# See http://biopython.org/wiki/Getting_Started for more information about bioPython
# More tutorials at http://biopython.org/DIST/docs/tutorial/Tutorial.html
# bioPython can be installed a number of ways, the simplest ways being:
# pip install biopython
# or
# sudo apt-get install python-biopython
# Install Documentation:
# sudo apt-get install python-biopython-doc
# You can install bioSQL with:
# sudo apt-get install python-biopython-sql


# Import bioPython
import Bio

from Bio.Seq import Seq

#create a sequence object
my_seq = Seq('CATGTAGACTAG')

#print out some details about it
print 'seq %s is %i bases long' % (my_seq, len(my_seq))
print 'reverse complement is %s' % my_seq.reverse_complement()
print 'protein translation is %s' % my_seq.translate()


