#!/usr/bin/env python2.7

# Allele Encoding Conversion by John Minton <cjohnweb@gmail.com>
# http://pythonjohn.com/
# Creative Commons Attribution License
# Originally developed on Debian
# Designed to work with raw 23andme.com data.

# This utility is designed to parse an RSID with associated Allels to generate a -/-, +/-. -/+, or +/+ result.
# Since this is different for each allel we need an index of these values to map the results 
# of your genetic expression.

# Pass in an RSID and Allel (2 letters either C's, G's, A's or T's) and get a ?/? result




